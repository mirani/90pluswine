package Master;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;


public class DataCall_Updated {

	public WebDriver driver;
	protected String username;
	protected String password;
	protected String fullname;
	protected String mobile;
	protected String firstname;
	protected String lastname;
	protected String email;
	protected String center;
	protected String TestResult;
	protected String TestReason;
	protected String todaysdate;
	protected String dateemail;
	protected String datemobile;
	protected String toemail;
	protected String contactcomment;
	protected int totalNoOfRows;
	protected String href;
	protected int row;
	protected int row1;
	protected int col;
	protected int col1;
	protected String testresult;
	protected String finalresult;
	protected String final_result;
	protected Sheet sh;
	protected Sheet sh1;
	protected Sheet sh2;
	protected Sheet sh3;
	protected Sheet sh4;
	private Logger logger;
	private Properties obj;
	private WebDriverWait wait;
	protected String price_range;
	protected String bottle_ship;
	protected String time_per_year;
	protected String streetaddress1;
	protected String streetaddress2;
	protected String city;
	protected String state;
	protected String zip;
	protected String cardname;
	protected String cardnumber;
	protected String expdate;
	protected String cvv;
	protected String zipcode;
	protected String screenshotname;
	protected String Red1;
	protected String White1;
	protected String Rose1;
	protected String Sparkling1;
	protected String RedMeats2;
	protected String Poultry2;
	protected String Seafood2;
	protected String French2;
	protected String Italian2;
	protected String Mexican2;
	protected String Japanese2;
	protected String Chinese2;
	protected String MiddleEastern2;
	protected String Indian2;
	protected String Coffee3;
	protected String Tea3;
	protected String FruitJuices3;
	protected String VegetableJuices3;
	protected String Vodka3;
	protected String Tequila3;
	protected String Beer3;
	protected String Whiskey3;
	protected String Rum3;
	protected String MixedDrink3;
	protected String Dark4;
	protected String Milk4;
	protected String White4;
	protected String Apple5;
	protected String Peach5;
	protected String Orange5;
	protected String Banana5;
	protected String Grapes5;
	protected String Berries5;
	protected String Rose6;
	protected String WetForest6;
	protected String Beach6;
	protected String Leather6;
	protected String Berries6;
	protected String Coffee6;
	protected String Citrus6;
	protected String Spicy7;
	protected String Salty7;
	protected String Sour7;
	protected String Sweet7;
	protected String GardenHerbs7;
	protected String Fruity8;
	protected String Dry8;
	protected String Spicy8;
	protected String LightBody8;
	protected String MediumBody8;
	protected String FullBody8;
	protected String Comment9;

//---------------------------------Taking Screen Shot---------------------------------------------------	
	public void getscreenshot() throws Exception,InterruptedException 
	{
	        File scrFile = ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.FILE);
	     
	        FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") +"//ScreenShot//" +screenshotname + ".png"));
	}

//---------------------------------Write Result in Excel Sheet--------------------------------------------
	public void setregisterresult() throws Exception,InterruptedException 
	{
		//Workbook  book = Workbook.getWorkbook(new File(System.getProperty("user.dir") + "\\TestData.xls"));
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet sheetToEdit = workbook.getSheet("Register");
	    Label l1 = new Label(5, row, TestResult);
	    sheetToEdit.addCell(l1);
	    Label l2 = new Label(6, row, TestReason);
	    sheetToEdit.addCell(l2);
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();		
	}
	public void setregisterdetails() throws Exception,InterruptedException 
	{
		//Workbook  book = Workbook.getWorkbook(new File(System.getProperty("user.dir") + "\\TestData.xls"));
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet sheetToEdit = workbook.getSheet("Register");
	    WritableSheet sheetToEdit1 = workbook.getSheet("StartTheQuiz");
	    WritableSheet sheetToEdit2 = workbook.getSheet("Shipping");
	    
	    Label l7 = new Label(1, row, "");
	    sheetToEdit.addCell(l7);
	    
	    Label l8 = new Label(2, row, "");
	    sheetToEdit.addCell(l8);
	    
	    Label l1 = new Label(1, row, dateemail);
	    sheetToEdit.addCell(l1);
	    
	    Label l2 = new Label(2, row, datemobile);
	    sheetToEdit.addCell(l2);
	    
	    Label l3 = new Label(0, row, dateemail);
	    sheetToEdit1.addCell(l3);
	    
	    Label l4 = new Label(0, row, dateemail);
	    sheetToEdit2.addCell(l4);
	    
	    Label l5 = new Label(5, row, "");
	    sheetToEdit.addCell(l5);
	    
	    Label l6 = new Label(6, row, "");
	    sheetToEdit.addCell(l6);
	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();		
	}
//-----------------------------------Write Result in CSV File---------------------------------------
	
    public void csvresult() throws Exception, IOException,InterruptedException
    {
        /*CsvWriter csvOutput = new CsvWriter(new FileWriter(System.getProperty("user.dir") + "//finalresult.csv", true),' ');
        csvOutput.write(finalresult);
        csvOutput.endRecord();              
        csvOutput.close();*/
        OutputStream fileStream = new BufferedOutputStream(new FileOutputStream(System.getProperty("user.dir") + "//finalresult.csv"));
        Writer outStreamWriter = new OutputStreamWriter(fileStream, StandardCharsets.UTF_8);
        @SuppressWarnings("resource")
		BufferedWriter buffWriter = new BufferedWriter(outStreamWriter);
        buffWriter.append(finalresult);
        buffWriter.flush();
    }
//-----------------------------------Driver Setup-----------------------------------------------------	
    public void driverset() throws Exception ,InterruptedException
	{
    	  System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "//geckodriver"); 
		  System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.Jdk14Logger");
		  setObj(new Properties());					
		  FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"//application.properties");
		  getObj().load(objfile);
		  System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
		  
		  setLogger(Logger.getLogger("Login"));
	      PropertyConfigurator.configure(System.getProperty("user.dir")+ "//Log4j.properties"); 
		  
		  LoggingPreferences loggingPrefs = new LoggingPreferences();
		  loggingPrefs.enable(LogType.BROWSER, Level.ALL);
		  loggingPrefs.enable(LogType.CLIENT, Level.ALL);
		  loggingPrefs.enable(LogType.DRIVER, Level.ALL);
		  loggingPrefs.enable(LogType.PERFORMANCE, Level.ALL);
		  loggingPrefs.enable(LogType.PROFILER, Level.ALL);
		  loggingPrefs.enable(LogType.SERVER, Level.ALL);
		  
		  DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		  desiredCapabilities.setCapability("marionette", true);
		  desiredCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		  desiredCapabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingPrefs);
		  
		  /*System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");	  
		  System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
		  FirefoxProfile profile = new FirefoxProfile();
		  profile.setPreference(FirefoxProfile.ALLOWED_HOSTS_PREFERENCE, "localhost");*/
		  
	      FirefoxOptions options = new FirefoxOptions();
	      options.merge(desiredCapabilities);
		  options.setHeadless(true);
		  //options.setBinary("/usr/bin/firefox");
		  options.setCapability("marionette", true);
		  options.addArguments("--start-maximized");
		  options.addArguments(System.getProperty("user.dir") + "//geckodriver");
		  options.addArguments("--proxy-server='direct://'");
		  options.addArguments("--proxy-bypass-list=*");
		  options.addArguments("--disable-extensions");
		  options.addArguments("--headless");
		  options.addArguments("--disable-gpu");
		  options.addArguments("--window-size=1440,900");
		  options.addArguments("--no-sandbox");
		  options.addArguments("--browsertime.xvfb");
		  options.setLogLevel(FirefoxDriverLogLevel.TRACE);
		  
		  setDriver(new FirefoxDriver(options));
		  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS); 

	      setWait(new WebDriverWait(getDriver(), 90));
	      getDriver().get(getObj().getProperty("URL"));
	      getDriver().manage().window().maximize();
	      getLogger().info("Browser Opened");
	      String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	      FileInputStream fs = new FileInputStream(FilePath);
	      Workbook wb = Workbook.getWorkbook(fs);
	      sh = wb.getSheet("Register");
	      sh1 = wb.getSheet("StartTheQuiz");
	      sh2 = wb.getSheet("Shipping");
	      sh3 = wb.getSheet("Validation_Result");
	      sh4 = wb.getSheet("Final_Result");
	      
	      SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyHHmm");  
	      Date date = new Date(); 
	      todaysdate = formatter.format(date);
	}
	public void deleteresultfile() throws Exception,InterruptedException
	{
		File f1 = new File(System.getProperty("user.dir")+ "//finalresult.csv");
		if(f1.isFile())
		{
			f1.delete();
		}
	}
	public void deletescreenshot() throws Exception,InterruptedException
	{
		File f1 = new File(System.getProperty("user.dir")+ "//ScreenShot");
		for(File file: f1.listFiles()) 
		if (!file.isDirectory()) 
		file.delete();
	}
	public WebDriver getDriver(){
		return driver;
	}

	public void setDriver(WebDriver driver) throws InterruptedException{
		this.driver = driver;
	}

	public WebDriverWait getWait() throws InterruptedException{
		return wait;
	}

	public void setWait(WebDriverWait wait) throws InterruptedException {
		this.wait = wait;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public Properties getObj() {
		return obj;
	}

	public void setObj(Properties obj) {
		this.obj = obj;
	}
	public void registerData() throws Exception,InterruptedException
	{
		fullname = sh.getCell(0,row).getContents();
		password = sh.getCell(3,row).getContents();
	}
	public void emailid() throws Exception,InterruptedException
	{
		String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	    FileInputStream fs = new FileInputStream(FilePath);
	    Workbook wb = Workbook.getWorkbook(fs);
	    sh = wb.getSheet("Register");
	}
	public void QuizQuestion() throws Exception,InterruptedException
	{
		email = sh1.getCell(0,row).getContents();
		password = sh1.getCell(1,row).getContents();
		Red1 = sh1.getCell(2,row).getContents();
		White1 = sh1.getCell(3,row).getContents();
		Rose1 = sh1.getCell(4,row).getContents();
		Sparkling1 = sh1.getCell(5,row).getContents();
		RedMeats2 = sh1.getCell(6,row).getContents();
		Poultry2 = sh1.getCell(7,row).getContents();
		Seafood2 = sh1.getCell(8,row).getContents();
		French2 = sh1.getCell(9,row).getContents();
		Italian2 = sh1.getCell(10,row).getContents();
		Mexican2 = sh1.getCell(11,row).getContents();
		Japanese2 = sh1.getCell(12,row).getContents();
		Chinese2 = sh1.getCell(13,row).getContents();
		MiddleEastern2 = sh1.getCell(14,row).getContents();
		Indian2 = sh1.getCell(15,row).getContents();
		Coffee3 = sh1.getCell(16,row).getContents();
		Tea3 = sh1.getCell(17,row).getContents();
		FruitJuices3 = sh1.getCell(18,row).getContents();
		VegetableJuices3 = sh1.getCell(19,row).getContents();
		Vodka3 = sh1.getCell(20,row).getContents();
		Tequila3 = sh1.getCell(21,row).getContents();
		Beer3 = sh1.getCell(22,row).getContents();
		Whiskey3 = sh1.getCell(23,row).getContents();
		Rum3 = sh1.getCell(24,row).getContents();
		MixedDrink3 = sh1.getCell(25,row).getContents();
		Dark4 = sh1.getCell(26,row).getContents();
		Milk4 = sh1.getCell(27,row).getContents();
		White4 = sh1.getCell(28,row).getContents();
		Apple5 = sh1.getCell(29,row).getContents();
		Peach5 = sh1.getCell(30,row).getContents();
		Orange5 = sh1.getCell(31,row).getContents();
		Banana5 = sh1.getCell(32,row).getContents();
		Grapes5 = sh1.getCell(33,row).getContents();
		Berries5 = sh1.getCell(34,row).getContents();
		Rose6 = sh1.getCell(35,row).getContents();
		WetForest6 = sh1.getCell(36,row).getContents();
		Beach6 = sh1.getCell(37,row).getContents();
		Leather6 = sh1.getCell(38,row).getContents();
		Berries6 = sh1.getCell(39,row).getContents();
		Coffee6 = sh1.getCell(40,row).getContents();
		Citrus6 = sh1.getCell(41,row).getContents();
		Spicy7 = sh1.getCell(42,row).getContents();
		Salty7 = sh1.getCell(43,row).getContents();
		Sour7 = sh1.getCell(44,row).getContents();
		Sweet7 = sh1.getCell(45,row).getContents();
		GardenHerbs7 = sh1.getCell(46,row).getContents();
		Fruity8 = sh1.getCell(47,row).getContents();
		Dry8 = sh1.getCell(48,row).getContents();
		Spicy8 = sh1.getCell(49,row).getContents();
		LightBody8 = sh1.getCell(50,row).getContents();
		MediumBody8 = sh1.getCell(51,row).getContents();
		FullBody8 = sh1.getCell(52,row).getContents();
		Comment9 = sh1.getCell(53,row).getContents();
	}
	
	public void shippingData() throws Exception,InterruptedException
	{
		email = sh2.getCell(0,row).getContents();
		password = sh2.getCell(1,row).getContents();
		price_range = sh2.getCell(2,row).getContents();
		bottle_ship = sh2.getCell(3,row).getContents();
		time_per_year = sh2.getCell(4,row).getContents();
		streetaddress1 = sh2.getCell(5,row).getContents();
		streetaddress2 = sh2.getCell(6,row).getContents();
		city = sh2.getCell(6,row).getContents();
		state = sh2.getCell(8,row).getContents();
		zip = sh2.getCell(9,row).getContents();
		cardname = sh2.getCell(10,row).getContents();
		cardnumber = sh2.getCell(11,row).getContents();
		expdate = sh2.getCell(12,row).getContents();
		cvv = sh2.getCell(13,row).getContents();
		zipcode = sh2.getCell(14,row).getContents();
	}
	public void validation_result() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Validation_Result");
	    
	    Label l1 = new Label(col,row,testresult);
	    resultsheet.addCell(l1);
	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_lv_clear() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(5,1,"Not Run");
	    resultsheet.addCell(l1);	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_fpv_clear() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(6,1,"Not Run");
	    resultsheet.addCell(l1);	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_rv_clear() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(0,1,"Not Run");
	    resultsheet.addCell(l1);	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_qv_clear() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(1,1,"Not Run");
	    resultsheet.addCell(l1);	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_sv_clear() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(2,1,"Not Run");
	    resultsheet.addCell(l1);	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_rc_clear() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(3,1,"Not Run");
	    resultsheet.addCell(l1);	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_cv_clear() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(4,1,"Not Run");
	    resultsheet.addCell(l1);	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_login_validation() throws Exception,InterruptedException
	{
		String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	    FileInputStream fs = new FileInputStream(FilePath);
	    Workbook wb = Workbook.getWorkbook(fs);  
		sh3 = wb.getSheet("Validation_Result");

		for (int row = 3; row < 8; row++) {
			final_result = sh3.getCell(14,row).getContents();
			if((final_result).contentEquals("Fail"))
			{
				
				col1 = 5;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
			else if ((final_result).contentEquals("Pass"))
			{
				col1 = 5;
				row1 = 1;	
				final_result = "Pass";
				this.final_result();				
			}
			else
			{
				col1 = 5;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
		}
	}
	public void final_forgot_pwd_validation() throws Exception,InterruptedException
	{
		String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	    FileInputStream fs = new FileInputStream(FilePath);
	    Workbook wb = Workbook.getWorkbook(fs);  
		sh3 = wb.getSheet("Validation_Result");

		for (int row = 17; row < 20; row++) {
			final_result = sh3.getCell(14,row).getContents();
			if((final_result).contentEquals("Fail"))
			{
				
				col1 = 6;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
			else if ((final_result).contentEquals("Pass"))
			{
				col1 = 6;
				row1 = 1;	
				final_result = "Pass";
				this.final_result();				
			}
			else
			{
				col1 = 6;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
		}
	}
	public void final_register_validation() throws Exception,InterruptedException
	{
		String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	    FileInputStream fs = new FileInputStream(FilePath);
	    Workbook wb = Workbook.getWorkbook(fs);  
		sh3 = wb.getSheet("Validation_Result");

		for (int row = 3; row < 13; row++) {
			final_result = sh3.getCell(2,row).getContents();
			if((final_result).contentEquals("Fail"))
			{
				
				col1 = 0;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
			else if ((final_result).contentEquals("Pass"))
			{
				col1 = 0;
				row1 = 1;	
				final_result = "Pass";
				this.final_result();				
			}
			else
			{
				col1 = 0;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
		}
	}
	public void final_contact_validation() throws Exception,InterruptedException
	{
		String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	    FileInputStream fs = new FileInputStream(FilePath);
	    Workbook wb = Workbook.getWorkbook(fs);  
		sh3 = wb.getSheet("Validation_Result");

		for (int row = 17; row < 20; row++) {
			final_result = sh3.getCell(10,row).getContents();
			if((final_result).contentEquals("Fail"))
			{
				
				col1 = 4;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
			else if ((final_result).contentEquals("Pass"))
			{
				col1 = 4;
				row1 = 1;	
				final_result = "Pass";
				this.final_result();				
			}
			else
			{
				col1 = 4;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
		}
	}
	public void final_result() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Final_Result");
	    
	    Label l1 = new Label(col1,row1,final_result);
	    resultsheet.addCell(l1);
	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void final_csv_result() throws Exception,InterruptedException
	{
		String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	    FileInputStream fs = new FileInputStream(FilePath);
	    Workbook wb = Workbook.getWorkbook(fs);  
		sh3 = wb.getSheet("Final_Result");
		for (col = 0; col < 7; col++) {
			final_result = sh3.getCell(col,1).getContents();
			System.out.println(final_result);
			if((final_result).contentEquals("Fail"))
			{
				finalresult = "Fail";
				break;
			}
			else if((final_result).contentEquals("Pass"))
			{
				finalresult = "Pass";
			}
			else
			{
				finalresult = "Fail";
				break;
			}
		}
		if(finalresult.contentEquals("Pass"))
		{
			finalresult = "Pass";
			csvresult();
		}
		else
		{
			finalresult = "Fail";
			csvresult();
		}

	}
	public void final_shipping_validation() throws Exception,InterruptedException
	{
		String FilePath = System.getProperty("user.dir") + "//90PlusWineTestData.xls";
	    FileInputStream fs = new FileInputStream(FilePath);
	    Workbook wb = Workbook.getWorkbook(fs);  
		sh4 = wb.getSheet("Validation_Result");
		for (int row = 3; row < 18; row++) {
			final_result = sh4.getCell(6,row).getContents();
			if((final_result).contentEquals("Fail"))
			{
				col1 = 2;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
			else if ((final_result).contentEquals("Pass"))
			{
				col1 = 2;
				row1 = 1;
				final_result = "Pass";
				this.final_result();	
			}
			else
			{
				col1 = 2;
				row1 = 1;
				final_result = "Fail";
				this.final_result();
				break;
			}
		}
	}
	public void register_result_clear() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Validation_Result");
	    
	    Label l1 = new Label(2,3,"");
	    resultsheet.addCell(l1);
	    Label l2 = new Label(2,4,"");
	    resultsheet.addCell(l2);
	    Label l3 = new Label(2,5,"");
	    resultsheet.addCell(l3);
	    Label l4 = new Label(2,6,"");
	    resultsheet.addCell(l4);
	    Label l5 = new Label(2,7,"");
	    resultsheet.addCell(l5);
	    Label l6 = new Label(2,8,"");
	    resultsheet.addCell(l6);
	    Label l7 = new Label(2,9,"");
	    resultsheet.addCell(l7);
	    Label l8 = new Label(2,10,"");
	    resultsheet.addCell(l8);
	    Label l9 = new Label(2,11,"");
	    resultsheet.addCell(l9);
	    Label l10 = new Label(2,12,"");
	    resultsheet.addCell(l10);
	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void contact_result_clear() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Validation_Result");
	    
	    Label l1 = new Label(10,17,"");
	    resultsheet.addCell(l1);
	    Label l2 = new Label(10,18,"");
	    resultsheet.addCell(l2);
	    Label l3 = new Label(10,19,"");
	    resultsheet.addCell(l3);
	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	public void shipment_result_clear() throws Exception,InterruptedException
	{
		Workbook existingWorkbook = Workbook.getWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")));
	    WritableWorkbook workbook = Workbook.createWorkbook(new File((System.getProperty("user.dir") + "//90PlusWineTestData.xls")), existingWorkbook);
	    WritableSheet resultsheet = workbook.getSheet("Validation_Result");
	    
	    Label l1 = new Label(6,3,"");
	    resultsheet.addCell(l1);
	    Label l2 = new Label(6,4,"");
	    resultsheet.addCell(l2);
	    Label l3 = new Label(6,5,"");
	    resultsheet.addCell(l3);
	    Label l4 = new Label(6,6,"");
	    resultsheet.addCell(l4);
	    Label l5 = new Label(6,7,"");
	    resultsheet.addCell(l5);
	    Label l6 = new Label(6,8,"");
	    resultsheet.addCell(l6);
	    Label l7 = new Label(6,9,"");
	    resultsheet.addCell(l7);
	    Label l8 = new Label(6,10,"");
	    resultsheet.addCell(l8);
	    Label l9 = new Label(6,11,"");
	    resultsheet.addCell(l9);
	    Label l10 = new Label(6,12,"");
	    resultsheet.addCell(l10);
	    Label l11 = new Label(6,13,"");
	    resultsheet.addCell(l11);
	    Label l12 = new Label(6,14,"");
	    resultsheet.addCell(l12);
	    Label l13 = new Label(6,15,"");
	    resultsheet.addCell(l13);
	    Label l14 = new Label(6,16,"");
	    resultsheet.addCell(l14);
	    Label l15 = new Label(6,17,"");
	    resultsheet.addCell(l15);
	    Label l16 = new Label(6,18,"");
	    resultsheet.addCell(l16);
	    
	    workbook.write();
	    workbook.close();
	    existingWorkbook.close();
	}
	
	
}
