package Execute;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import Register.register_unique;

public class Run_Register_Unique {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { register_unique.class });
		testng.addListener(tla);
		testng.run();
	}
}
