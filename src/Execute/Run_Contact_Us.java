package Execute;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import General.contact_us;

public class Run_Contact_Us {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		TestListenerAdapter tla4 = new TestListenerAdapter();
		TestNG testng4 = new TestNG();
		testng4.setTestClasses(new Class[] { contact_us.class });
		testng4.addListener(tla4);
		testng4.run();
	}
}