package Execute;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import Runnable.Shipment_Validation;

public class Run_Shipment_Validation {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { Shipment_Validation.class });
		testng.addListener(tla);
		testng.run();
	}
}