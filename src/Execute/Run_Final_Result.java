package Execute;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import Master.Final_Result;

public class Run_Final_Result {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		TestListenerAdapter tla4 = new TestListenerAdapter();
		TestNG testng4 = new TestNG();
		testng4.setTestClasses(new Class[] { Final_Result.class });
		testng4.addListener(tla4);
		testng4.run();
	}
}