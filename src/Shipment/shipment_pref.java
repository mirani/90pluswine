package Shipment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import Master.Field_Master;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class shipment_pref extends Field_Master {
	
@BeforeTest
public void befortest() throws Exception {
		driverset();
}	

@Test
public void StartQuiz() throws Exception, Throwable {
			totalNoOfRows = sh2.getRows();
			for (row = 1; row < totalNoOfRows; row++) {
			shippingData();					
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login"))).click();
				getLogger().info("Login Link open successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login Link not opened.";
				getLogger().info(TestReason);
				screenshotname = "Login_Link_Not_Opened_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_link"))).click();
				getLogger().info("Login link clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Login link not found.";
				getLogger().info(TestReason);
				screenshotname = "Login_Link_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(email);
				getLogger().info("Email Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Email field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("password"))));
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).sendKeys(password);
				getLogger().info("Password Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Password_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("Login_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("Login_button"))).click();
				getLogger().info("Login Now button clicked.");
			}
			catch(Exception e)
			{
				TestReason = "Login Now button not found";
				getLogger().info(TestReason);
				screenshotname = "Loginn_Now_Button_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			Thread.sleep(3000);
			boolean visible = getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).isDisplayed();
			if (visible == true)
			{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				Thread.sleep(1000);
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz_pop"))).click();
			}
			Thread.sleep(3000);
			try
			{		
				winepricerange();
			}
			catch(Exception e)
			{
				TestReason = "Price Range option not found";
				getLogger().info(TestReason);
				screenshotname = "Price_Range_Option_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{		
				noofbottleship();
			}
			catch(Exception e)
			{
				TestReason = "No of Bottle option not found";
				getLogger().info(TestReason);
				screenshotname = "No_of_Bottle_Option_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{		
				nooftimeship();
			}
			catch(Exception e)
			{
				TestReason = "No of Times Bottle option not found";
				getLogger().info(TestReason);
				screenshotname = "No_of_Times_Bottle_Option_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("shipnext"))));
				getDriver().findElement(By.xpath(getObj().getProperty("shipnext"))).click();
				getLogger().info("Wine Preference Done");
				
			}
			catch(Exception e)
			{
				TestReason = "Wine Preference has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Wine_Preference_Not_Done_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			Thread.sleep(2000);	
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("add_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("add_address_button"))).click();
				getLogger().info("Address option open to enter");
				
			}
			catch(Exception e)
			{
				TestReason = "Address option not open.";
				getLogger().info(TestReason);
				screenshotname = "Add_Address_option_button_not_found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("address1"))));
				getDriver().findElement(By.xpath(getObj().getProperty("address1"))).sendKeys(streetaddress1);
				getLogger().info("Address1 Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Address1 field not found";
				getLogger().info(TestReason);
				screenshotname = "Address1_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("address2"))));
				getDriver().findElement(By.xpath(getObj().getProperty("address2"))).sendKeys(streetaddress2);
				getLogger().info("Address2 Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Address2 field not found";
				getLogger().info(TestReason);
				screenshotname = "Address2_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("city"))));
				getDriver().findElement(By.xpath(getObj().getProperty("city"))).sendKeys(city);
				getLogger().info("City Inserted");
			}
			catch(Exception e)
			{
				TestReason = "City field not found";
				getLogger().info(TestReason);
				screenshotname = "City_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("state"))));
				getDriver().findElement(By.xpath(getObj().getProperty("state"))).click();
			    WebElement dropdownValue = driver.findElement(By.xpath("//div[contains(text(),'"+state+"')]"));
			    dropdownValue.click();
				getLogger().info("State Selected");

			}
			catch(Exception e)
			{
				TestReason = "State field not found";
				getLogger().info(TestReason);
				screenshotname = "State_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("zip"))));
				getDriver().findElement(By.xpath(getObj().getProperty("zip"))).sendKeys(zip);
				getLogger().info("Zip Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Zip field not found";
				getLogger().info(TestReason);
				screenshotname = "Zip_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Address Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cardname"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cardname"))).sendKeys(cardname);
				getLogger().info("Cardname Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Cardname field not found";
				getLogger().info(TestReason);
				screenshotname = "Cardname_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cardnumber"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cardnumber"))).sendKeys(cardnumber);
				getLogger().info("Card Number Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Card Number field not found";
				getLogger().info(TestReason);
				screenshotname = "Cardnumber_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("expdate"))));
				getDriver().findElement(By.xpath(getObj().getProperty("expdate"))).sendKeys(expdate);
				getLogger().info("Exp Date Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Exp Date field not found";
				getLogger().info(TestReason);
				screenshotname = "ExpDate_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cvv"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cvv"))).sendKeys(cvv);
				getLogger().info("CVV Inserted");
			}
			catch(Exception e)
			{
				TestReason = "CVV field not found";
				getLogger().info(TestReason);
				screenshotname = "CVV_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("zipcode"))));
				getDriver().findElement(By.xpath(getObj().getProperty("zipcode"))).sendKeys(zipcode);
				getLogger().info("Zipcode Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Zipcode field not found";
				getLogger().info(TestReason);
				screenshotname = "Zipcode_Field_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			Thread.sleep(3000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("completeorder"))));
				getDriver().findElement(By.xpath(getObj().getProperty("completeorder"))).click();
				getLogger().info("Completed order Success");
			}
			catch(Exception e)
			{
				TestReason = "Complete order button not found";
				getLogger().info(TestReason);
				screenshotname = "Complete_Order_button_Not_Found_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("shippre_edit"))));
				getDriver().findElement(By.xpath(getObj().getProperty("shippre_edit"))).click();
				TestResult = "Pass";
				TestReason = "Shipment_Pref Completed Successfully";
				getLogger().info(TestReason);
				setregisterresult();				
			}
			catch(Exception e)
			{
				TestReason = "Shipment_Pref not completed Successfully.";
				getLogger().info(TestReason);
				screenshotname = "Shipment_Prefnot_not_found_Home_TC_No_" + row;
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
				break;
			}						
		}
}
@AfterTest
public void aftertest() {
   getDriver().close();
}
}