package Shipment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import Master.Field_Master;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class Shipment_pref_validation extends Field_Master {
	
@BeforeTest
public void befortest() throws Exception {
		driverset();
}	
@Test (priority = 1)
public void Wine_Preference_Price_Range_Validate() throws Exception, Throwable {

			dateemail = todaysdate + "@mailinator.com";
			datemobile = todaysdate;
			row = 1;
			setregisterdetails();
			registerData();
			shippingData();
			signup();
			age_verify();
			signup_link();
			userfullname();
			userdateemailaddress();
			userdatemobile();
			userpassword();
			terms_accept();
			registerbutton();										
			Thread.sleep(3000);
			
			boolean visible = getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).isDisplayed();
			if (visible == true)
			{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				Thread.sleep(1000);
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz_pop"))).click();
			}
			Thread.sleep(3000);

			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("shipnext"))));
				getDriver().findElement(By.xpath(getObj().getProperty("shipnext"))).click();
				getLogger().info("Next button pressed without fillup any wine preference");				
			}
			catch(Exception e)
			{
				TestReason = "Next button not loaded.";
				getLogger().info(TestReason);
				screenshotname = "Wine_Preference_Next_Button_Not_Load";
				getscreenshot();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("shipment_msg"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("shipment_msg"))).getText();
				if(txtmsg.contentEquals("Please ensure price range, shipment size, and shipment frequency selections have been completed."))
				{
					winepricerange();
				}	
				getLogger().info("1st message display of Wine Preference");	
			}
			catch(Exception e)
			{
				TestReason = "1st message not display of Wine Preference";
				getLogger().info(TestReason);
				screenshotname = "1st_message_not_display_Wine_Preference";
				getscreenshot();	
			}			
}			
@Test (priority = 2)
public void Wine_Preference_Bottle_Per_Ship_Validate() throws Exception, Throwable {
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("shipnext"))));
				getDriver().findElement(By.xpath(getObj().getProperty("shipnext"))).click();
				getLogger().info("Next button pressed after select Price Range");
			}
			catch(Exception e)
			{
				TestReason = "Next button not loaded after price range selected.";
				getLogger().info(TestReason);
				screenshotname = "Wine_Preference_Next_Button_Not_Load_after_price_select";
				getscreenshot();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("shipment_msg"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("shipment_msg"))).getText();
				if(txtmsg.contentEquals("Please ensure price range, shipment size, and shipment frequency selections have been completed."))
				{
					noofbottleship();
				}	
				getLogger().info("2nd message display of Wine Preference");	
			}
			catch(Exception e)
			{
				TestReason = "2nd message not display of Wine Preference";
				getLogger().info(TestReason);
				screenshotname = "2nd_message_not_display_Wine_Preference";
				getscreenshot();	
			}
}
@Test (priority = 3)
public void Wine_Preference_Shipment_Per_Year_Validate() throws Exception, Throwable {
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("shipnext"))));
				getDriver().findElement(By.xpath(getObj().getProperty("shipnext"))).click();
				getLogger().info("Next button pressed after select Price Range");				
			}
			catch(Exception e)
			{
				TestReason = "Next button not loaded after bottle receive per shipment selected.";
				getLogger().info(TestReason);
				screenshotname = "Wine_Preference_Next_Button_Not_Load_after_bottle_receive_select";
				getscreenshot();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("shipment_msg"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("shipment_msg"))).getText();
				if(txtmsg.contentEquals("Please ensure price range, shipment size, and shipment frequency selections have been completed."))
				{
					nooftimeship();
				}	
				getLogger().info("3rd message display of Wine Preference");	
			}
			catch(Exception e)
			{
				TestReason = "3rd message not display of Wine Preference";
				getLogger().info(TestReason);
				screenshotname = "3rd_message_not_display_Wine_Preference";
				getscreenshot();	
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("shipnext"))));
				getDriver().findElement(By.xpath(getObj().getProperty("shipnext"))).click();
				getLogger().info("Next button pressed after Wine preference selected");
				
			}
			catch(Exception e)
			{
				TestReason = "Next button not loaded after wine preference selected.";
				getLogger().info(TestReason);
				screenshotname = "Wine_Preference_Next_Button_Not_Load_after_selection";
				getscreenshot();
			}
			Thread.sleep(2000);
}			
@Test(priority = 4)
public void Address1_Validate() throws Exception, Throwable {
			shippingData();
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("add_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("add_address_button"))).click();
				getLogger().info("Address option open to enter");	
			}
			catch(Exception e)
			{
				TestReason = "Address option not open.";
				getLogger().info(TestReason);
				screenshotname = "Add_Address_option_button_not_found";
				getscreenshot();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Save Address Press without Address 1 Enter");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found";
				getscreenshot();
			}
			String address1_blank = getDriver().findElement(By.xpath(getObj().getProperty("address1_blank"))).getText();
			
			if(address1_blank.equals("This field is required"))
			{
				getLogger().info("Address1 Validation Pass");
			}
			else
			{
				getLogger().info("Address1 Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("address1"))));
				getDriver().findElement(By.xpath(getObj().getProperty("address1"))).sendKeys(streetaddress1);
				getLogger().info("Address1 Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Address1 field not found";
				getLogger().info(TestReason);
				screenshotname = "Address1_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
			}
}			
@Test(priority = 5)
public void City_Validate() throws Exception, Throwable {
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Save Address Press without Address 1 Enter");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found";
				getscreenshot();
			}
			String cityblank = getDriver().findElement(By.xpath(getObj().getProperty("city_blank"))).getText();
			
			if(cityblank.equals("This field is required"))
			{
				getLogger().info("City Validation Pass");
			}
			else
			{
				getLogger().info("City Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("city"))));
				getDriver().findElement(By.xpath(getObj().getProperty("city"))).sendKeys(city);
				getLogger().info("City Inserted");
			}
			catch(Exception e)
			{
				TestReason = "City field not found";
				getLogger().info(TestReason);
				screenshotname = "City_Field_Not_Found";
				getscreenshot();
			}
}			
@Test(priority = 6)
public void State_Validate() throws Exception, Throwable {			
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Save Address Press without State Enter");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found";
				getscreenshot();
			}
			String stateblank = getDriver().findElement(By.xpath(getObj().getProperty("state_blank"))).getText();
			
			if(stateblank.equals("This field is required"))
			{
				getLogger().info("State Validation Pass");
			}
			else
			{
				getLogger().info("State Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("state"))));
				getDriver().findElement(By.xpath(getObj().getProperty("state"))).click();
			    WebElement dropdownValue = driver.findElement(By.xpath("//div[contains(text(),'"+state+"')]"));
			    dropdownValue.click();
				getLogger().info("State Selected");
			}
			catch(Exception e)
			{
				TestReason = "State field not found";
				getLogger().info(TestReason);
				screenshotname = "State_Field_Not_Found";
				getscreenshot();
			}
}
@Test(priority = 7)
public void Address_Zipcode_Validate() throws Exception, Throwable {
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Save Address Press without Zipcode Enter");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found";
				getscreenshot();
			}
			String zipcodeblank = getDriver().findElement(By.xpath(getObj().getProperty("zipcode_blank"))).getText();
			
			if(zipcodeblank.equals("This field is required"))
			{
				getLogger().info("Zipcode Validation Pass");
			}
			else
			{
				getLogger().info("Zipcode Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("zip"))));
				getDriver().findElement(By.xpath(getObj().getProperty("zip"))).sendKeys(zip);
				getLogger().info("Zip Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Zip field not found";
				getLogger().info(TestReason);
				screenshotname = "Zip_Field_Not_Found";
				getscreenshot();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("save_address_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("save_address_button"))).click();
				getLogger().info("Address Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save address button not found";
				getLogger().info(TestReason);
				screenshotname = "Save_Address_button_Not_Found";
				getscreenshot();
			}
		}
@Test (priority = 8)
public void Credit_Card_Name_Validate() throws Exception, Throwable {
			shippingData();
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
			String cardnameblank = getDriver().findElement(By.xpath(getObj().getProperty("cardname_blank"))).getText();
			
			if(cardnameblank.equals("This field is required"))
			{
				getLogger().info("Require Card Name Validation Pass");
			}
			else
			{
				getLogger().info("Require Card Name Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cardname"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cardname"))).sendKeys(cardname);
				getLogger().info("Cardname Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Cardname field not found";
				getLogger().info(TestReason);
				screenshotname = "Cardname_Field_Not_Found";
				getscreenshot();
			}
}
@Test (priority = 9)
public void Credit_Card_Number_Validate() throws Exception, Throwable {
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
			String cardnumberblank = getDriver().findElement(By.xpath(getObj().getProperty("cardnumber_blank"))).getText();
			
			if(cardnumberblank.equals("This field is required"))
			{
				getLogger().info("Require Card Number Validation Pass");
			}
			else
			{
				getLogger().info("Require Card Number Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cardnumber"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cardnumber"))).sendKeys("123");
				getLogger().info("Invalid Card Number Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Card Number field not found";
				getLogger().info(TestReason);
				screenshotname = "Card_Number_Field_Not_Found";
				getscreenshot();
			}
			String cardnumberinvalid = getDriver().findElement(By.xpath(getObj().getProperty("cardnumber_invalid"))).getText();
			
			if(cardnumberinvalid.equals("Enter valid card number"))
			{
				getLogger().info("Invalid Card Number Validation Pass");
			}
			else
			{
				getLogger().info("Invalid Card Number Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cardnumber"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cardnumber"))).clear();
				getDriver().findElement(By.xpath(getObj().getProperty("cardnumber"))).sendKeys(cardnumber);
				getLogger().info("Card Number Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Card Number field not found";
				getLogger().info(TestReason);
				screenshotname = "Cardnumber_Field_Not_Found";
				getscreenshot();
			}
			
}
@Test (priority = 10)
public void Exp_Date_Validate() throws Exception, Throwable {
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
			String expdateblank = getDriver().findElement(By.xpath(getObj().getProperty("expdate_blank"))).getText();
			
			if(expdateblank.equals("This field is required"))
			{
				getLogger().info("Require Exp Date Validation Pass");
			}
			else
			{
				getLogger().info("Require Exp Date Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("expdate"))));
				getDriver().findElement(By.xpath(getObj().getProperty("expdate"))).sendKeys("abc");
				getLogger().info("Invalid Exp Date Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Exp Date field not found";
				getLogger().info(TestReason);
				screenshotname = "Exp_Date_Field_Not_Found";
				getscreenshot();
			}
			String expdateinvalid = getDriver().findElement(By.xpath(getObj().getProperty("expdate_invalid"))).getText();
			
			if(expdateinvalid.equals("Enter valid expiry date"))
			{
				getLogger().info("Invalid Exp Date Validation Pass");
			}
			else
			{
				getLogger().info("Invalid Exp Date Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("expdate"))));
				getDriver().findElement(By.xpath(getObj().getProperty("expdate"))).clear();
				getDriver().findElement(By.xpath(getObj().getProperty("expdate"))).sendKeys(expdate);
				getLogger().info("Exp Date Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Exp Date field not found";
				getLogger().info(TestReason);
				screenshotname = "ExpDate_Field_Not_Found";
				getscreenshot();
			}
			
}
@Test (priority = 11)
public void CVV_Validate() throws Exception, Throwable {
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
			String cvvblank = getDriver().findElement(By.xpath(getObj().getProperty("cvv_blank"))).getText();
	
			if(cvvblank.equals("Enter valid cvv"))
			{
				getLogger().info("Require CVV Validation Pass");
			}
			else
			{
				getLogger().info("Require CVV Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cvv"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cvv"))).sendKeys("abcd");
				getLogger().info("Invalid CVV Inserted");
			}
			catch(Exception e)
			{
				TestReason = "CVV field not found";
				getLogger().info(TestReason);
				screenshotname = "CVV_Field_Not_Found";
				getscreenshot();
				TestResult = "Fail";
				setregisterresult();
			}
			String cvvinvalid = getDriver().findElement(By.xpath(getObj().getProperty("cvv_blank"))).getText();
			
			if(cvvinvalid.equals("Enter valid cvv"))
			{
				getLogger().info("Invalid CVV Validation Pass");
			}
			else
			{
				getLogger().info("Invalid CVV Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("cvv"))));
				getDriver().findElement(By.xpath(getObj().getProperty("cvv"))).clear();
				getDriver().findElement(By.xpath(getObj().getProperty("cvv"))).sendKeys(cvv);
				getLogger().info("Valid CVV Inserted");
			}
			catch(Exception e)
			{
				TestReason = "CVV field not found";
				getLogger().info(TestReason);
				screenshotname = "CVV_Field_Not_Found";
				getscreenshot();
			}
}
@Test (priority = 12)
public void Bill_Zip_Validate() throws Exception, Throwable {
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
			String billzipblank = getDriver().findElement(By.xpath(getObj().getProperty("bill_zip_blank"))).getText();
	
			if(billzipblank.equals("This field is required"))
			{
				getLogger().info("Require Bill Zip Validation Pass");
			}
			else
			{
				getLogger().info("Require Bill Zip Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("zipcode"))));
				getDriver().findElement(By.xpath(getObj().getProperty("zipcode"))).sendKeys("123");
				getLogger().info("Invalid Zipcode Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Zipcode field not found";
				getLogger().info(TestReason);
				screenshotname = "Zipcode_Field_Not_Found";
				getscreenshot();
			}
			String billzipinvalid = getDriver().findElement(By.xpath(getObj().getProperty("bill_zip_blank"))).getText();
			
			if(billzipinvalid.equals("This field is required"))
			{
				getLogger().info("Invalid Bill Zip Validation Pass");
			}
			else
			{
				getLogger().info("Invalid Bill Zip Validation Fail");
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("zipcode"))));
				getDriver().findElement(By.xpath(getObj().getProperty("zipcode"))).clear();
				getDriver().findElement(By.xpath(getObj().getProperty("zipcode"))).sendKeys(zipcode);
				getLogger().info("Zipcode Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Zipcode field not found";
				getLogger().info(TestReason);
				screenshotname = "Zipcode_Field_Not_Found";
				getscreenshot();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("savecard"))));
				getDriver().findElement(By.xpath(getObj().getProperty("savecard"))).click();
				getLogger().info("Creditcard Save Success");
			}
			catch(Exception e)
			{
				TestReason = "Save Creditcard button not found";
				getLogger().info(TestReason);
				screenshotname = "Creditcard_Not_Found";
				getscreenshot();
			}
}
@AfterTest
public void aftertest() {
   getDriver().close();
}
	}