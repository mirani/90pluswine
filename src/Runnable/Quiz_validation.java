package Runnable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import Master.DataCall;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class Quiz_validation extends DataCall {
	
@BeforeTest
public void befortest() throws Exception,InterruptedException {
		driverset();
}	

@Test ()
public void StartQuiz() throws Exception, Throwable, InterruptedException {

			fullname = "Tech Holding";
			email = todaysdate + "@mailinator.com";
			mobile = todaysdate;
			password = "123456";
			final_qv_clear();
			Thread.sleep(3000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("sign_up"))));
				getDriver().findElement(By.xpath(getObj().getProperty("sign_up"))).click();
				getLogger().info("Sign_Up Link open successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Sign_Up Link not opened.";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Link_Not_Opened";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("age_verify"))));
				getDriver().findElement(By.xpath(getObj().getProperty("age_verify"))).click();
				getLogger().info("Age verification successfully done.");
			}
			catch(Exception e)
			{
				TestReason = "Age verification not done.";
				getLogger().info(TestReason);
				screenshotname = "Age_Verification_Not_Done";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			Thread.sleep(3000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("sign_up_link"))));
				getDriver().findElement(By.xpath(getObj().getProperty("sign_up_link"))).click();
				getLogger().info("Sign Up link clicked successfully.");
			}
			catch(Exception e)
			{
				TestReason = "Sign Up link not found.";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Link_Not_Found";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("full_name"))));
				getDriver().findElement(By.xpath(getObj().getProperty("full_name"))).sendKeys(fullname);
				getLogger().info("Full Name Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Full Name field not found";
				getLogger().info(TestReason);
				screenshotname = "FullName_Field_Not_Found";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("email"))));
				getDriver().findElement(By.xpath(getObj().getProperty("email"))).sendKeys(email);
				getLogger().info("Email Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Email field not found";
				getLogger().info(TestReason);
				screenshotname = "Email_Field_Not_Found";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("mobile"))));
				getDriver().findElement(By.xpath(getObj().getProperty("mobile"))).sendKeys(mobile);
				getLogger().info("Mobile No Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Mobile field not found";
				getLogger().info(TestReason);
				screenshotname = "Mobile_Field_Not_Found";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("password"))));
				getDriver().findElement(By.xpath(getObj().getProperty("password"))).sendKeys(password);
				getLogger().info("Password Inserted");
			}
			catch(Exception e)
			{
				TestReason = "Password field not found";
				getLogger().info(TestReason);
				screenshotname = "Password_Field_Not_Found";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.className(getObj().getProperty("term_accept"))));
				getDriver().findElement(By.className(getObj().getProperty("term_accept"))).click();
				getLogger().info("Agree Terms and Condition");
			}
			catch(Exception e)
			{
				TestReason = "Terms & Condition option not found";
				getLogger().info(TestReason);
				screenshotname = "Terms_Condition_Not_Found";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("register_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("register_button"))).click();
				getLogger().info("Sign Up Now button clicked.");
			}
			catch(Exception e)
			{
				TestReason = "Sign Up button not found";
				getLogger().info(TestReason);
				screenshotname = "Sign_Up_Button_Not_Found";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
						
			Thread.sleep(4000);
			
//----------------------Start The Quiz Main------------------------------			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Start The Quiz Skip link pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Start The Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Start_Quiz_link_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			Thread.sleep(1000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Start Quiz Continue button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Start Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Start_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			Thread.sleep(1000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("start_quiz_button"))));
				getDriver().findElement(By.xpath(getObj().getProperty("start_quiz_button"))).click();
				getLogger().info("User has just Start The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Start The Quiz has not been done.";
				getLogger().info(TestReason);
				screenshotname = "Quiz_not_Started";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			Thread.sleep(5000);
			
//----------------------------Step 1 Quiz---------------------------------------			
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step1 Quiz Skip link pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step1 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_link_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step1 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step1 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step1 Quiz Exit button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Start The Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Exit_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step1 Quiz Continue button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step1 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step1 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step1 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step1 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step1 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step1 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step1 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			Thread.sleep(1000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step1 Skip of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step1 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step1_Quiz_Skip_Not_Done";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			System.out.println("Step1 Completed successfully");
			Thread.sleep(3000);
//-------------------------Step 2 Quiz--------------------------------
						
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step2 Quiz Skip link pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step2 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_link_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			Thread.sleep(1000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step2 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step2 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step2 Quiz Exit button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step2 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Exit_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step2 Quiz Continue button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step2 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step2 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step2 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step2 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step2 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step2 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step2 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step2 Skip of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step2 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step2_Quiz_Skip_Not_Done";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			System.out.println("Step2 Completed successfully");
			Thread.sleep(1000);

//-------------------------Step 3 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step3 Quiz Skip link pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step3 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_link_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step3 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step3 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step3 Quiz Exit button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step3 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Exit_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step3 Quiz Continue button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step3 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step3 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step3 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step3 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step3 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step3 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step3 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step3 Skip of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step3 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step3_Quiz_Skip_Not_Done";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			System.out.println("Step3 Completed successfully");
			Thread.sleep(1000);
//-------------------------Step 4 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step4 Quiz Skip link pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step4 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_link_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step4 Quiz Go back button pressed");		
			}
			catch(Exception e)
			{
				TestReason = "Step4 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step4 Quiz Exit button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step4 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Exit_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step4 Quiz Continue button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step4 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step4 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step4 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step4 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step4 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step4 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step4 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step4 Skip of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step4 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step4_Quiz_Skip_Not_Done";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			System.out.println("Step4 Completed successfully");
			Thread.sleep(1000);
			
//-------------------------Step 5 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step5 Quiz Skip link pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step5 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_link_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step5 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step5 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step5 Quiz Exit button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step5 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Exit_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step5 Quiz Continue button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step5 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step5 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step5 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step5 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step5 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step5 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step5 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step5 Skip of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step5 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step5_Quiz_Skip_Not_Done";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			System.out.println("Step5 Completed successfully");
			Thread.sleep(1000);
			
//-------------------------Step 6 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step6 Quiz Skip link pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step6 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_link_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step6 Quiz Go back button pressed");
				
			}
			catch(Exception e)
			{
				TestReason = "Step6 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step6 Quiz Exit button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step6 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Exit_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step6 Quiz Continue button pressed");
			}
			catch(Exception e)
			{
				TestReason = "Step6 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step6 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step6 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step6 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step6 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step6 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step6 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step6 Skip of The Quiz");
			}
			catch(Exception e)
			{
				TestReason = "Step6 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step6_Quiz_Skip_Not_Done";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			System.out.println("Step6 Completed successfully");
			Thread.sleep(1000);
			
//-------------------------Step 7 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step7 Quiz Skip link pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step7 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_link_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step7 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step7 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step7 Quiz Exit button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step7 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Exit_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step7 Quiz Continue button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step7 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step7 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step7 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step7 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step7 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step7 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step7 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step7 Skip of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step7 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step7_Quiz_Skip_Not_Done";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			System.out.println("Step7 Completed successfully");
			Thread.sleep(1000);
			
//-------------------------Step 8 Quiz--------------------------------
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step8 Quiz Skip link pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step8 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_link_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step8 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step8 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step8 Quiz Exit button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step8 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Exit_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step8 Quiz Continue button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step8 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step8 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step8 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step8 Quiz Go back button pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step8 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("next_question"))));
				getDriver().findElement(By.xpath(getObj().getProperty("next_question"))).click();
				getLogger().info("Step8 Completed of The Quiz");	
			}
			catch(Exception e)
			{
				TestReason = "Step8 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("leave_ans"))));
				String txtmsg = getDriver().findElement(By.xpath(getObj().getProperty("leave_ans"))).getText();
				if(txtmsg.contentEquals("Did you mean to leave this answer blank?"))
				{
				getDriver().findElement(By.xpath(getObj().getProperty("skip_yes"))).click();
				}
				getLogger().info("Step8 Skip of The Quiz");		
			}
			catch(Exception e)
			{
				TestReason = "Step8 not skip of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step8_Quiz_Skip_Not_Done";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			System.out.println("Step8 Completed successfully");
			Thread.sleep(3000);
			
//-------------------------Step 9 Quiz--------------------------------
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_quiz"))).click();
				getLogger().info("Step9 Quiz Skip link pressed");	
			}
			catch(Exception e)
			{
				TestReason = "Step9 Quiz skip link not found";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_link_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("skip_goback"))));
				getDriver().findElement(By.xpath(getObj().getProperty("skip_goback"))).click();
				getLogger().info("Step9 Quiz Go back button pressed");
			}
			catch(Exception e)
			{
				TestReason = "Step9 Quiz Go back button not found";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("exit_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("exit_quiz"))).click();
				getLogger().info("Step9 Quiz Exit button pressed");			
			}
			catch(Exception e)
			{
				TestReason = "Step9 Quiz Exit button not found";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_Exit_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			Thread.sleep(2000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("continue_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("continue_quiz"))).click();
				getLogger().info("Step9 Quiz Continue button pressed");		
			}
			catch(Exception e)
			{
				TestReason = "Step9 Quiz Continue button not found";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}	
			Thread.sleep(3000);
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("finish_quiz"))));
				getDriver().findElement(By.xpath(getObj().getProperty("finish_quiz"))).click();
				getLogger().info("Step9 Completed of The Quiz");
				
			}
			catch(Exception e)
			{
				TestReason = "Step9 not completed of The Quiz";
				getLogger().info(TestReason);
				screenshotname = "Step9_Quiz_Button_Not_Click";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			System.out.println("Step9 Completed successfully");
			Thread.sleep(3000);
			
//---------------------------------------------Quiz Confirmation-----------------------------------------------
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("quizcomplete"))));
				getDriver().findElement(By.xpath(getObj().getProperty("shipmentpref"))).click();
				TestResult = "Pass";
				TestReason = "Quiz Completed Successfully";
				getLogger().info(TestReason);
								
			}
			catch(Exception e)
			{
				TestReason = "Quiz not completed Successfully.";
				getLogger().info(TestReason);
				screenshotname = "User_Not_Redirect_Home";
				getscreenshot();
				col = 10;
				row = 3;
				testresult = "Fail";
				validation_result();
				driver.quit();
			}
			col = 10;
			row = 3;
			testresult = "Pass";
			validation_result();
			
			col1 = 1;
			row1 = 1;
			final_result = "Pass";
			final_result();
					
			
		}
@AfterTest
public void aftertest() throws InterruptedException {
   getDriver().quit();
}
}