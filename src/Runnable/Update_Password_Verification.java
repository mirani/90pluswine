package Runnable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import Master.Field_Master;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class Update_Password_Verification extends Field_Master {
	
@BeforeTest
public void befortest() throws Exception,InterruptedException {
		driverset();	
}	

@Test (priority = 1)
public void Updated_Password_Verification() throws Exception, Throwable,InterruptedException {
			row = 1;
			registerData();
			accountsettingdata();
			Thread.sleep(3000);			
			login_button();
			Thread.sleep(1000);
			login_link();
			Thread.sleep(1000);
			updateduseremailaddress();
			Thread.sleep(1000);
			updateduserpassword();			
			Thread.sleep(1000);
			login_now_button();
			Thread.sleep(2000);
			
			try {
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("password_verify"))));
				String currentpassword = driver.findElement(By.xpath(getObj().getProperty("password_verify"))).getText();
				
				if (currentpassword.contentEquals("Please verify your email address Resend Email")) {
					getLogger().info("Updated Password Verification Successfully Done.");
					col = 2;
					row = 31;
					testresult = "Pass";
					validation_result();
				} else {
					getLogger().info("Updated Password Verification Not Successfully Done.");
					col = 2;
					row = 31;
					testresult = "Fail";
					validation_result();
					Assert.fail();
				}
			} catch (Exception e) {
				TestReason = "Updated Password Verification Failed";
				getLogger().info(TestReason);
				screenshotname = "Updated_Password_Verification_Failed";
				getscreenshot();
				col = 2;
				row = 31;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}			
}
@AfterTest
public void aftertest() throws InterruptedException {
   getDriver().quit();
}
	}