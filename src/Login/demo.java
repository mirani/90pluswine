package Login;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import Master.DataCall;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class demo extends DataCall {
	
@BeforeTest
public void befortest() throws Exception {
		driverset();
}	

@Test
public void first() throws Exception, Throwable {
			username = "test1@tester.com";
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='nav-accounts__link'][contains(.,'Sign In / Sign Up')]")));
				getDriver().findElement(By.xpath("//div[@class='nav-accounts__link'][contains(.,'Sign In / Sign Up')]")).click();
				getLogger().info("Account Link open successfully.");
			}
			catch(Exception e)
			{
				getLogger().info("Account Link not opened.");
				screenshotname = "Account_Link_Not_Opened_TC_No_";
				getscreenshot();
				
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.id(getObj().getProperty("username"))));
				getDriver().findElement(By.id(getObj().getProperty("username"))).sendKeys(username);
				getLogger().info("Username Inserted");
			}
			catch(Exception e)
			{
				getLogger().info("Username not found");
				screenshotname = "Username_Not_Found_TC_No_";
				getscreenshot();
			
			}
			
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@type='submit'][contains(.,'Continue')]")));
				getDriver().findElement(By.xpath("//button[@type='submit'][contains(.,'Continue')]")).click();
				getLogger().info("User Logged In");
			}
			
			catch(Exception e)
			{
				getLogger().info("Login Link not found");
				screenshotname = "Login_Link_Not_Found_TC_No_";
				getscreenshot();
				
			}
			

}
@AfterTest
public void aftertest() {
   getDriver().close();
}
}