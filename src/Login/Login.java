package Login;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import Master.DataCall;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
public class Login extends DataCall {
	
@BeforeTest
public void befortest() throws Exception {
		driverset();
}	

@Test
public void first() throws Exception, Throwable {
			totalNoOfRows = sh.getRows();
			for (row = 1; row < totalNoOfRows; row++) {
			username = sh.getCell(0,row).getContents();
			password = sh.getCell(1,row).getContents();
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='account']/a")));
				getDriver().findElement(By.xpath(".//*[@id='account']/a")).click();
				getLogger().info("Account Link open successfully.");
			}
			catch(Exception e)
			{
				getLogger().info("Account Link not opened.");
				screenshotname = "Account_Link_Not_Opened_TC_No_" + row;
				getscreenshot();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.id(getObj().getProperty("username"))));
				getDriver().findElement(By.id(getObj().getProperty("username"))).sendKeys(username);
				getLogger().info("Username Inserted");
			}
			catch(Exception e)
			{
				getLogger().info("Username not found");
				screenshotname = "Username_Not_Found_TC_No_" + row;
				getscreenshot();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.visibilityOfElementLocated(By.id(getObj().getProperty("password"))));
				getDriver().findElement(By.id(getObj().getProperty("password"))).sendKeys(password);
				getLogger().info("Password Inserted");
			}
			catch(Exception e)
			{
				getLogger().info("Password not found");
				screenshotname = "Password_Not_Found_TC_No_" + row;
				getscreenshot();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.id("login")));
				getDriver().findElement(By.id("login")).click();
				getLogger().info("User Logged In");
			}
			
			catch(Exception e)
			{
				getLogger().info("Login Link not found");
				screenshotname = "Login_Link_Not_Found_TC_No_" + row;
				getscreenshot();
				break;
			}
			try
			{
				getWait().until(ExpectedConditions.elementToBeClickable(By.id(getObj().getProperty("Logout"))));
				getDriver().findElement(By.id(getObj().getProperty("Logout"))).click();
			}
			catch(Exception e)
			{
				getLogger().info("Logout Link not found");
				screenshotname = "Logout_Link_Not_Found_TC_No_" + row;
				getscreenshot();
				break;
			}
			TestResult = "Pass";
			setregisterresult();
}
}
@AfterTest
public void aftertest() {
   getDriver().close();
}
}