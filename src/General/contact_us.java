package General;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import Master.DataCall;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class contact_us extends DataCall {

	@BeforeTest
	public void befortest() throws Exception {
		driverset();
	}

	@Test(priority = 1, description = "Mandatory Field Validation")
	public void contactMandatoryValidate() throws Exception {
		fullname = "Tech Holding";
		email = todaysdate + "@mailinator.com";
		contactcomment = todaysdate + "Cras commodo magna a nulla blandit, in sodales justo venenatis. Proin hendrerit rhoncus molestie. Vestibulum libero odio, cursus pulvinar augue quis, aliquam rhoncus turpis. Sed id tortor in turpis semper sagittis. Maecenas lobortis turpis sed justo feugiat, nec auctor purus efficitur. Mauris hendrerit, sapien posuere cursus condimentum, dui orci efficitur sapien, eget efficitur diam lorem non dui. Vestibulum sodales sapien nibh, eget consectetur elit facilisis ut. Nam euismod nisl ut iaculis mattis. Quisque ac purus lobortis, faucibus tortor et, pulvinar ipsum";
		contact_result_clear();
		final_cv_clear();
		Thread.sleep(2000);
		try {
			getWait().until(ExpectedConditions.elementToBeClickable(By.className(getObj().getProperty("my_account"))));
			getDriver().findElement(By.className(getObj().getProperty("my_account"))).click();
			getLogger().info("My Account Link Click");
		} catch (Exception e) {
			TestReason = "My Account Link not found";
			getLogger().info(TestReason);
			screenshotname = "My_Account_Link_Not_Found";
			getscreenshot();
			col = 10;
			row = 17;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
		try {
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("contactlink"))));
			getDriver().findElement(By.xpath(getObj().getProperty("contactlink"))).click();
			TestReason = "Contact Us Link Clicked";
			getLogger().info(TestReason);
		} catch (Exception e) {
			TestReason = "Contact Us link not found";
			getLogger().info(TestReason);
			screenshotname = "Register_Not_Success";
			getscreenshot();
			col = 10;
			row = 17;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
		Thread.sleep(1000);
		try {
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("contactsubmit"))));
			getDriver().findElement(By.xpath(getObj().getProperty("contactsubmit"))).click();
			getLogger().info("Contact Submit button clicked.");
		} catch (Exception e) {
			TestReason = "Contact Submit button not found";
			getLogger().info(TestReason);
			screenshotname = "Contact_Submit_Button_Not_Found";
			getscreenshot();
			col = 10;
			row = 17;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
		Thread.sleep(2000);
		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("namerequiremsg"))));
			String namerequiremsg = driver.findElement(By.xpath(getObj().getProperty("namerequiremsg"))).getText();
			
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("emailrequiremsg"))));
			String emailrequiremsg = driver.findElement(By.xpath(getObj().getProperty("emailrequiremsg"))).getText();
			
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("commentrequiremsg"))));
			String commentrequiremsg = driver.findElement(By.xpath(getObj().getProperty("commentrequiremsg"))).getText();
			
			if (namerequiremsg.contentEquals("This field is required.") && emailrequiremsg.contentEquals("This field is required.") && commentrequiremsg.contentEquals("This field is required.")) {
				getLogger().info("Mandatory Validation Verify Successfully");
				col = 10;
				row = 17;
				testresult = "Pass";
				validation_result();
			} else {
				getLogger().info("Mandatory Validation Not Verify");
				col = 10;
				row = 17;
				testresult = "Fail";
				validation_result();
				Assert.fail();
			}
		} catch (Exception e) {
			TestReason = "Mandatory Validation Failed";
			getLogger().info(TestReason);
			screenshotname = "Mandatory_Validation_Failed";
			getscreenshot();
			col = 10;
			row = 17;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
	}

	@Test(priority = 2, description = "Contact Us Invalid Email Address Verified Successfully")
	public void invalidEmailAddress() throws Exception {
		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("contactfullname"))));
			getDriver().findElement(By.xpath(getObj().getProperty("contactfullname"))).clear();
			getDriver().findElement(By.xpath(getObj().getProperty("contactfullname"))).sendKeys(fullname);
			getLogger().info("Full Name Inserted");
		} catch (Exception e) {
			TestReason = "Full Name field not found";
			getLogger().info(TestReason);
			screenshotname = "FullName_Field_Not_Found";
			getscreenshot();
			col = 10;
			row = 18;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("contactemail"))));
			getDriver().findElement(By.xpath(getObj().getProperty("contactemail"))).clear();
			getDriver().findElement(By.xpath(getObj().getProperty("contactemail"))).sendKeys("invalidemail");
			getLogger().info("Email Inserted");
		} catch (Exception e) {
			TestReason = "Email field not found";
			getLogger().info(TestReason);
			screenshotname = "Email_Field_Not_Found";
			getscreenshot();
			col = 10;
			row = 18;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
		try {
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("contactsubmit"))));
			getDriver().findElement(By.xpath(getObj().getProperty("contactsubmit"))).click();
			getLogger().info("Contact Submit button clicked.");
		} catch (Exception e) {
			TestReason = "Contact Submit button not found";
			getLogger().info(TestReason);
			screenshotname = "Contact_Submit_Button_Not_Found";
			getscreenshot();
			col = 10;
			row = 18;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("contactinvalidemailmsg"))));
			String invalidemailmsg = driver.findElement(By.xpath(getObj().getProperty("contactinvalidemailmsg"))).getText();
			if (invalidemailmsg.contentEquals("Email address is invalid.")) {
				getLogger().info("Contact US Invalid Email Address Verify Successfully");
				col = 10;
				row = 18;
				testresult = "Pass";
				validation_result();
			} else {
				getLogger().info("Contact US Invalid Email Address Not Verify");
				col = 10;
				row = 18;
				testresult = "Fail";
				validation_result();
			}
		} catch (Exception e) {
			TestReason = "Invalid Email Address Validation Failed";
			getLogger().info(TestReason);
			screenshotname = "Invalid_Email_Not_Verify";
			getscreenshot();
			col = 10;
			row = 18;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
	}

	@Test(priority = 3)
	public void contactSubmitSuccess() throws Exception {
		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("contactemail"))));
			getDriver().findElement(By.xpath(getObj().getProperty("contactemail"))).clear();
			Thread.sleep(1000);
			getDriver().findElement(By.xpath(getObj().getProperty("contactemail"))).sendKeys(email);
			getLogger().info("Email Inserted");
		} catch (Exception e) {
			TestReason = "Email field not found";
			getLogger().info(TestReason);
			screenshotname = "Email_Field_Not_Found";
			getscreenshot();
			col = 10;
			row = 19;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("contactcomment"))));
			getDriver().findElement(By.xpath(getObj().getProperty("contactcomment"))).sendKeys(contactcomment);
			getLogger().info("Contact Us Comment Inserted");
		} catch (Exception e) {
			TestReason = "Contact US Comment field not found";
			getLogger().info(TestReason);
			screenshotname = "Contact_Comment_Field_Not_Found";
			getscreenshot();
			col = 10;
			row = 19;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
		try {
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("contactsubmit"))));
			getDriver().findElement(By.xpath(getObj().getProperty("contactsubmit"))).click();
			getLogger().info("Contact Submit button clicked.");
		} catch (Exception e) {
			TestReason = "Contact Submit button not found";
			getLogger().info(TestReason);
			screenshotname = "Contact_Submit_Button_Not_Found";
			getscreenshot();
			col = 10;
			row = 19;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("contactsuccess"))));
			String contactMsgText = driver.findElement(By.xpath(getObj().getProperty("contactsuccess"))).getText();
			if (contactMsgText.contentEquals("Thank you for contacting us, We will get back to you soon...")) {
				getLogger().info("Contact US Verify Successfully");
				col = 10;
				row = 19;
				testresult = "Pass";
				validation_result();
			} else {
				getLogger().info("Contact US Not Verify Properly");
				col = 10;
				row = 19;
				testresult = "Fail";
				validation_result();
			}
		} catch (Exception e) {
			TestReason = "Contact Us not completed.";
			getLogger().info(TestReason);
			screenshotname = "Contact_US_Not_Complete";
			getscreenshot();
			col = 10;
			row = 19;
			testresult = "Fail";
			validation_result();
			Assert.fail();
		}
	}

@AfterTest
public void aftertest() throws Exception {
		getDriver().quit();
	}
}