package General;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import Master.DataCall;

public class broken_image extends DataCall {
	@BeforeTest
	public void befortest() throws Exception {
		driverset();
	}

	@Test
	public void Link_Verify() throws Exception, Throwable {
		Thread.sleep(5000);
		// driver.navigate().to("https://dev.90pluswine.com/collection");
		try {
			List<WebElement> links = driver.findElements(By.tagName("img"));
			System.out.println("Total No of Images are : " + links.size());
			int brokenImagesCount = 0;
			for (int i = 0; i < links.size(); i++) {

				String linkURL = links.get(i).getAttribute("src");
				System.out.println(links.get(i).getText());
				URL url = new URL(linkURL);
				System.out.println(url);
				HttpURLConnection http = (HttpURLConnection) url.openConnection();
				// http.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64)
				// AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
				http.setConnectTimeout(10000);
				http.setReadTimeout(20000);
				int statusCode = http.getResponseCode();
				System.out.println(statusCode);
				if (statusCode == 404 || statusCode == 500) {
					brokenImagesCount = brokenImagesCount + 1;
					System.out.println(linkURL + "and its Status codes is:" + statusCode);
				}
			}
			System.out.println("total number of broken images are: " + brokenImagesCount);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	@AfterTest
	public void aftertest() {
		getDriver().quit();
	}
}