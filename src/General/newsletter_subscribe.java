package General;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import Master.DataCall;

public class newsletter_subscribe extends DataCall {

	@BeforeTest
	public void befortest() throws Exception {
		driverset();
	}

	@Test
	public void subscribe() throws Exception, Throwable {

		center = "Trumbull";
		firstname = "QAUser";
		email = "qa@gustr.com";

		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("subscribe_link"))));
			getDriver().findElement(By.xpath(getObj().getProperty("subscribe_link"))).click();
			getLogger().info("Subscription Newsletter Link open successfully.");
		} catch (Exception e) {
			TestReason = "Sign In_Up Link not opened.";
			getLogger().info(TestReason);
			screenshotname = "Subscribe_Link_Not_Opened";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
		}
		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("center"))));
			getDriver().findElement(By.xpath(getObj().getProperty("center"))).sendKeys(center);
			getLogger().info("Center Inserted");
		} catch (Exception e) {
			TestReason = "Center field not found";
			getLogger().info(TestReason);
			screenshotname = "Center_Field_Not_Found_";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
		}
		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(.,'" + center + "')]")));
			getDriver().findElement(By.xpath("//h2[contains(.,'" + center + "')]")).click();
			getLogger().info("Center Selected");
		} catch (Exception e) {
			TestReason = "Center not found";
			getLogger().info(TestReason);
			screenshotname = "Center_Not_Found";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
		}
		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("first_name"))));
			getDriver().findElement(By.xpath(getObj().getProperty("first_name"))).sendKeys(firstname);
			getLogger().info("FirstName Inserted");
		} catch (Exception e) {
			TestReason = "FirstName field not found";
			getLogger().info(TestReason);
			screenshotname = "FirstName_Field_Not_Found";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
		}
		try {
			getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(getObj().getProperty("subscribe_email"))));
			getDriver().findElement(By.xpath(getObj().getProperty("subscribe_email"))).sendKeys(email);
			getLogger().info("Email Inserted");
		} catch (Exception e) {
			TestReason = "Email field not found";
			getLogger().info(TestReason);
			screenshotname = "Email_Field_Not_Found";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
		}
		try {
			getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("subscribe_submit"))));
			getDriver().findElement(By.xpath(getObj().getProperty("subscribe_submit"))).click();
			getLogger().info("Submit Button Clicked");
		} catch (Exception e) {
			TestReason = "Submit Button not found";
			getLogger().info(TestReason);
			screenshotname = "Submit_Button_Not_Found";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
		}
		getWait().until(ExpectedConditions.elementToBeClickable(By.xpath(getObj().getProperty("subscribe_edit"))));
		boolean enable = getDriver().findElement(By.xpath(getObj().getProperty("subscribe_edit"))).isEnabled();
		if (enable == true) {
			TestResult = "Pass";
			TestReason = "Subscription Successfully Done";
			getLogger().info(TestReason);
			setregisterresult();
		} else {
			TestReason = "Subscription not success";
			getLogger().info(TestReason);
			screenshotname = "Subscription_Not_Success";
			getscreenshot();
			TestResult = "Fail";
			setregisterresult();
		}
	}

	@AfterTest
	public void aftertest() {
		getDriver().close();
	}
}
